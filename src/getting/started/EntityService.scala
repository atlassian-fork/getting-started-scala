package getting.started

/**
 * Start point of all validations
 */
object EntityService {

  /**
   * Entity is in valid state only if it's within the range of 0 - 20
   */
  private val VALID_ENTITY_RANGE = Range(0, 20)

  /**
   * public entry. This will validate the input then delegate all the work to the manager. The actual validation won't be
   * executed here. They all will be delegated to centralized method in the same class.
   */
  def processEntity(entityOpt: Option[Entity]): Either[EntityError, Entity] = ???

  /**
   * Centralize all of validation here
   */
  private def isValidEntity(entityOpt: Option[Entity]): Either[EntityError, Entity] = ???

  /**
   * Simple check if the id is inside the range. Given, entity is the *real one
   */
  private def isValidEntityId(entity: Entity): Boolean = ???

}
